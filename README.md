# ProtocolValidator

#### 介绍
比较老式的协议验证工具，是字节流方式的协议。协议内容是Char，Short，int，String等类型。
针对协议做内容格式验证，防止客户端恶意发包。
程序包括2部分：协议验证器和协议验证文件编辑器。
真正的应用部分是c++协议验证器的协议验证部分的代码。应用的游戏服务器中。进行防止客户端伪造发包。是对老式通信协议结构的保护（老式协议没有验证，逻辑处理不全。很多外挂或者发包工具都会伪造往服务器发包。导致服务器不稳定。）。

#### 软件架构
1. 协议验证器C++编写
目录：ValidatorTest

2. 协议验证文件编辑器Java编写
目录：ValidatorEditor
使用javafx进行界面编程。主要是对XML文件的读写操作。



#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
package perfect.tool.xml;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class Utils {
	public static void warningAlert(String title, String header, String content) {
		Alert saveAlert = new Alert(AlertType.WARNING);
		saveAlert.setTitle(title);
		saveAlert.setHeaderText(header);
		saveAlert.setContentText(content);
		
		saveAlert.showAndWait();
	}
}

package perfect.tool.xml;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import perfect.tool.log.Log;

public class XmlManager {
	
	private String fileName = "vf.xml";
	
	private Document document = null;
	
	private Element elementProtocols = null;

	private static XmlManager gXmlManager = new XmlManager();
	public static XmlManager getMe() {
		return gXmlManager;
	}
	
	public void load() {
		SAXReader reader = new SAXReader();
		try {
			document = reader.read(new File(fileName));
			Element rootElement = document.getRootElement();
			
			Log.debug("root:{}", rootElement.getName());
			
			elementProtocols = rootElement.element("PROTOCOLS");
			
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void saveXml() {
		try {
			// Programme 2
			XMLWriter writer = new XMLWriter(new OutputStreamWriter(new FileOutputStream(fileName), "UTF-8"));
			writer.write(document);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public Document getDocument() {
		return document;
	}
	
	public Element getElementProtocols() {
		return elementProtocols;
	}
	
	public static void main(String args[]) {
		XmlManager.getMe().load();
		
	}
}

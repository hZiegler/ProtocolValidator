package perfect.tool.validator;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class ValidatorEditor extends Application {

    @Override
	public void start(Stage primaryStage) {
    	
    	ValidatorController root = new ValidatorController();

    	Scene scene = new Scene(root);
//		scene.getStylesheets().add(getClass().getClassLoader().getResource("application.css").toExternalForm());
		primaryStage.setScene(scene);
		primaryStage.setTitle("ValidatorEditor");
		primaryStage.show();
		primaryStage.setResizable(true);
		
		primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent event) {
				root.onCloseRequest(event);
			}
		});
	}

	public static void main(String[] args) {
        launch(args);
    }
}

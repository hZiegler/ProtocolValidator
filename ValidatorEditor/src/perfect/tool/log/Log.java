package perfect.tool.log;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.message.ParameterizedMessage;

/**
 * @author ziegler
 * 
 */
public class Log {
	/** 日志时间格式 */
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	private static List<LogAppender> appenders = new ArrayList<>();
	
	static {
		addAppender(new FileAppender(new File("editor.log")));
	}
	
	public static void addAppender(LogAppender logAppender) {
		appenders.add(logAppender);
	}
	
	public static void debug(String str, Object...params) {
//		write("DEBUG", str, params);
	}
	
	public static void info(String str, Object...params) {
		write("INFO", str, params);
	}
	
	public static void warn(String str, Object...params) {
		write("WARN", str, params);
	}
	
	public static void error(String str, Object...params) {
		write("ERROR", str, params);
	}
	
	private static void write(String prefix, String str, Object...params) {
		String content = ParameterizedMessage.format(str, params);
		String timeStr = sdf.format(new Date());
		content = new StringBuilder().append("[").append(timeStr).append("] ")
				.append("[").append(prefix).append("] ")
				.append(content).toString();
		
		System.out.println(content);
		
		for (LogAppender appender : appenders) {
			appender.write(content);
		}
	}

}

package perfect.tool.log;

public interface LogAppender {
	void write(String content);
}

package perfect.tool.log;

import javafx.scene.control.ListView;

public class ListViewAppender implements LogAppender {

	private ListView<String> listViewAppender;
	
	public ListViewAppender(ListView<String> listViewAppender) {
		this.listViewAppender = listViewAppender;
	}
	
	@Override
	public void write(String content) {
		listViewAppender.getItems().add(content);
		listViewAppender.scrollTo(listViewAppender.getItems().size());
	}

}

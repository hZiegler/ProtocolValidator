package perfect.tool.log;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FileAppender implements LogAppender {

	private static String LINE_SEPARATOR = System.getProperty("line.separator");
	
	private FileWriter fw;
	
	public FileAppender(File file) {
		super();

		try {
			if (!file.exists()) {
				file.createNewFile();
			}
			
			fw = new FileWriter(file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void write(String content) {
		try {
			fw.write(content);
			fw.write(LINE_SEPARATOR);
			fw.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}

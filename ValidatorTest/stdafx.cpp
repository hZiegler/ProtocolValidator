
// stdafx.cpp : 只包括标准包含文件的源文件
// ValidatorTest.pch 将作为预编译标头
// stdafx.obj 将包含预编译类型信息

#include "stdafx.h"

void SplitString(const string& s, vector<string>& v, const string& c)
{
	string::size_type pos1, pos2;
	pos2 = s.find(c);
	pos1 = 0;
	while (string::npos != pos2)
	{
		v.push_back(s.substr(pos1, pos2 - pos1));

		pos1 = pos2 + c.size();
		pos2 = s.find(c, pos1);
	}
	if (pos1 != s.length())
		v.push_back(s.substr(pos1));
}

//////////////////////////////////////////////////////////   
//   
//功能：十六进制转为十进制   
//   
//输入：const unsigned char *hex         待转换的十六进制数据   
//      int length                       十六进制数据长度   
//   
//输出：   
//   
//返回：int  rslt                        转换后的十进制数据   
//   
//思路：十六进制每个字符位所表示的十进制数的范围是0 ~255，进制为256   
//      左移8位(<<8)等价乘以256   
//   
/////////////////////////////////////////////////////////   
unsigned long HextoDec(const unsigned char *hex, int length)
{
	int i;
	unsigned long rslt = 0;
	for (i = 0; i < length; i++)
	{
		rslt += (unsigned long)(hex[i]) << (8 * (length - 1 - i));

	}
	return rslt;
}

// ValidatorTestDlg.h: 头文件
//

#pragma once


// CValidatorTestDlg 对话框
class CValidatorTestDlg : public CDialogEx
{
// 构造
public:
	CValidatorTestDlg(CWnd* pParent = nullptr);	// 标准构造函数

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_VALIDATORTEST_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	CString m_strValue;

	char m_szBuf[8192];
	afx_msg void OnBnClickedClearLog();
	CListBox m_listBox;
	virtual BOOL DestroyWindow();
};

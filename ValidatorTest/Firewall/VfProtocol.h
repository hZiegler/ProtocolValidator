#ifndef __VFPROTOCOL__H_
#define __VFPROTOCOL__H_

#include "VfTypes.h"

class IVfProtocol
{
private:
	IVfProtocol() {}

public:
	virtual ~IVfProtocol() 
	{
		int n = 0;
	}

public:
	IVfProtocol(int nProtocolId) { m_nProtocolId = nProtocolId; }

public:
	virtual int Check(TCHAR *pBuf, int length) = 0;

protected:
	int m_nProtocolId;
};

class VfProtocolIgnore : public IVfProtocol
{
public:
	VfProtocolIgnore(int nProtocolId) : IVfProtocol(nProtocolId) {}

public:
	virtual int Check(TCHAR *pBuf, int length);
};

class VfProtocolOnlyLength : public IVfProtocol, public VfTypeLimit
{
public:
	VfProtocolOnlyLength(int nProtocolId) : IVfProtocol(nProtocolId) {}

public:
	virtual int Check(TCHAR *pBuf, int length);

protected:
	virtual EnumVfType GetType() { return VfEmpty; }
	virtual int Check(int &index, TCHAR *pBuf, int length);
};

class VfProtocolRoot : public IVfProtocol
{
public:
	VfProtocolRoot(int nProtocolId) : IVfProtocol(nProtocolId) {}
	~VfProtocolRoot();

public:
	// ��������
	void LoadProtocol(TiXmlElement *pElmDATA);

protected:
	void SetVfTypes(vector<IVfType *> vtVfType);

public:
	virtual int Check(TCHAR *pBuf, int length);

protected:
	vector<IVfType *> m_vtVfType;
};

class VfProtocolIf : public VfProtocolRoot
{
public:
	VfProtocolIf() : VfProtocolRoot(0) {}

public:
	virtual int Check(int &index, TCHAR *pBuf, int length);
};

class VfProtocolElseIf : public VfProtocolRoot
{
public:
	VfProtocolElseIf() : VfProtocolRoot(0) {}

public:
	virtual int Check(int &index, TCHAR *pBuf, int length);
};

class VfProtocolFor : public VfProtocolRoot
{
public:
	VfProtocolFor() : VfProtocolRoot(0) {}

public:
	virtual int Check(int &index, TCHAR *pBuf, int length);
};



#endif
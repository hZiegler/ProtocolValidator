#include "StdAfx.h"
#include "VfProtocol.h"
#include "Trace.h"
#include "FirewallManager.h"


int VfProtocolIgnore::Check( TCHAR *pBuf, int length )
{
	return OK;
}

int VfProtocolOnlyLength::Check( TCHAR *pBuf, int length )
{
	// 验证协议内容长度是否在规定范围内
	int retVal = OK;
	int index = 0;

	retVal = Check(index, pBuf, length);

	return retVal;
}

int VfProtocolOnlyLength::Check( int &index, TCHAR *pBuf, int length )
{
	// 有效数据长度需要去掉最后1字节
	return VfTypeLimit::Check(length - 1, index, pBuf, length);
}

VfProtocolRoot::~VfProtocolRoot()
{
	for (int i=0; i<(int)m_vtVfType.size(); ++i)
	{
		IVfType *pVfType = m_vtVfType.at(i);
		if (pVfType != NULL)
		{
			delete pVfType;
			pVfType = NULL;
			m_vtVfType.at(i) = NULL;
		}
	}

	m_vtVfType.clear();
}

void VfProtocolRoot::SetVfTypes( vector<IVfType *> vtVfType )
{
	this->m_vtVfType = vtVfType;
}

void VfProtocolRoot::LoadProtocol( TiXmlElement *pElmDATA )
{
	vector<IVfType *> vtVfType;

	// 协议数据
	for (TiXmlElement *pElmType = pElmDATA->FirstChildElement(); NULL != pElmType;
		pElmType = pElmType->NextSiblingElement())
	{
		string strTypeName = pElmType->Value();

		if ("INT" == strTypeName)
		{
			VfTypeInt *pVfType = new VfTypeInt();
			pVfType->ParseVfTypeLimit(m_nProtocolId, pElmType);

			vtVfType.push_back(pVfType);
		}
		else if ("SHORT" == strTypeName)
		{
			VfTypeShort *pVfType = new VfTypeShort();
			pVfType->ParseVfTypeLimit(m_nProtocolId, pElmType);

			vtVfType.push_back(pVfType);
		}
		else if ("BYTE" == strTypeName)
		{
			VfTypeByte *pVfType = new VfTypeByte();
			pVfType->ParseVfTypeLimit(m_nProtocolId, pElmType);

			vtVfType.push_back(pVfType);
		}
		else if ("DWORD" == strTypeName)
		{
			VfTypeDWord *pVfType = new VfTypeDWord();
			pVfType->ParseVfTypeLimit(m_nProtocolId, pElmType);

			vtVfType.push_back(pVfType);
		}
		else if ("STRING" == strTypeName)
		{
			VfTypeString *pVfType = new VfTypeString();
			pVfType->ParseVfTypeLimit(m_nProtocolId, pElmType);

			vtVfType.push_back(pVfType);
		}
		else
		{
			LOGError("协议[%d]验证字段类型[%s]错误", m_nProtocolId, strTypeName);
		}
	}

	SetVfTypes(vtVfType);
}

int VfProtocolRoot::Check( TCHAR *pBuf, int length )
{
	int index = 0;

	for (int i=0; i<(int)m_vtVfType.size(); ++i)
	{
		int nErrCode = m_vtVfType[i]->Check(index, pBuf, length);
		if (OK != nErrCode)
		{
			return nErrCode;
		}
	}

	// 最后多余一个结束字节
	if (length != index + 1)
	{
		return ERRCODE_LENGTH;
	}

	return OK;
}

int VfProtocolIf::Check( int &index, TCHAR *pBuf, int length )
{
	for (int i=0; i<(int)m_vtVfType.size(); ++i)
	{
		int nErrCode = m_vtVfType[i]->Check(index, pBuf, length);
		if (OK != nErrCode)
		{
			return nErrCode;
		}
	}

	return OK;
}

int VfProtocolElseIf::Check( int &index, TCHAR *pBuf, int length )
{
	for (int i=0; i<(int)m_vtVfType.size(); ++i)
	{
		int nErrCode = m_vtVfType[i]->Check(index, pBuf, length);
		if (OK != nErrCode)
		{
			return nErrCode;
		}
	}

	return OK;
}

int VfProtocolFor::Check( int &index, TCHAR *pBuf, int length )
{
	for (int i=0; i<(int)m_vtVfType.size(); ++i)
	{
		int nErrCode = m_vtVfType[i]->Check(index, pBuf, length);
		if (OK != nErrCode)
		{
			return nErrCode;
		}
	}

	return OK;
}

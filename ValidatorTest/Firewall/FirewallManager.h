#pragma once

#include <map>
using namespace std;
#include "VfProtocol.h"

class USER;

enum ENUM_ACTION
{
	EMPTY			= 0,	//无动作
	ACTION_LOG		= 1,	//日志
	ACTION_KICK		= 2,	//踢掉并日志
	ACTION_FILTER_LOG		= 3,	//过滤有日志
	ACTION_FILTER_NO_LOG	= 4,	//过滤没有日志
};

enum ENUM_CHECK_ERRCODE
{
	OK						= 0,
	ERRCODE_NO_PROTOCOL		= 1,	//没有找到验证函数
	ERRCODE_LENGTH			= 2,	//长度错误
	ERRCODE_OUT_OF_RANGE		= 3,	//不在有效范围
	ERRCODE_FRENQUENCY		= 4,	//收包过于频繁
};

class FirewallManager
{
	typedef int (FirewallManager::*CheckFunc)(TCHAR *pBuf, int length);

public:
	FirewallManager(void);
	~FirewallManager(void);
	
	static FirewallManager& Instance()
	{
		static FirewallManager __firewallManager;
		return __firewallManager;
	}

public:
	// 协议防火墙启动状态
	static volatile bool g_running;
	
public:
	void Init();
	// 检查协议,BUF=数据内容开始位置，去掉4字节顺序和1字节协议编号
	// length=数据内容长度，也是去掉4字节顺序和1字节协议编号
	int Check(BYTE bCommand, char *pBuf, int length, USER *pUser);

	// 设置错误码对应动作
	void SetErrcodeAction(map<int, int> mapErrcodeAction, map<int, std::string> mapErrcodeDesc);
	const map<int, int> & GetErrcodeAction() { return m_mapErrcodeAction; }
	int GetActionByErrcode(int nErrcode);
	std::string GetDescByErrcode(int nErrcode);

	bool IsExistVfProtocol(int nProtocolId);
	// 添加一个验证协议
	bool AddVfProtocol(int nProtocolId, IVfProtocol *pVfProtocol);

	// 清空所有协议
	void ClearAllProtocol();

	void SetFrequency(bool bOpen, int nInterval, int nPackageCount);

protected:
	int CheckFrenquency(USER *pUser);


private:
	int Check_GAME_START_REQ(char *pBuf, int length);

private:
	// 注册检查协议
	void Register(int key, CheckFunc pFunc);

	int CheckProtocol(BYTE bCommand, char *pBuf, int length, USER *pUser);

private:
	map<int, CheckFunc> m_mapCheckFunc;

	// 
	bool m_bOpenFrequency;
	
	int m_nInterval;
	int m_nMaxPackageCount;
	
	// 错误码对应动作
	map<int, int> m_mapErrcodeAction;

	map<int, std::string> m_mapErrcodeDesc;

	// 验证协议
	map<int, IVfProtocol*> m_mapProtocol;
};

BOOL WriteArrayToLog(TCHAR *pData,int iDataLen);
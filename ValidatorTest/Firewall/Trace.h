/****************************************************************************
* Copyright (c) 2005
* All rights reserved.
* 
* 文件名称：PubFunc.h
* 摘    要：一些常用公共函数
* 作    者：lunatic
* 创建日期：2011-7-7
* 修改日志：
****************************************************************************/
#ifndef __TRACE__
#define __TRACE__

#pragma   warning(disable:4996)
#include <string>
#include <iostream>
#include <fstream>
#include <Windows.h>

#include <afxwin.h>

namespace Validator 
{


	class CTrace
	{
	public:
		CTrace()
		{
			InitializeCriticalSection(&m_crit);
		}

		~CTrace()
		{
			DeleteCriticalSection(&m_crit);
		}

		static CTrace& Instance()
		{
			static CTrace g_instance;
			return g_instance;
		}

	public:
		CListBox *m_pListBox = NULL;


		void Trace(const std::string &strDbgFileName, const char* szFormat, ...)
		{
			char szTime[128];

			SYSTEMTIME s;
			EnterCriticalSection(&m_crit);
			GetLocalTime(&s);
			_snprintf(szTime, 128, "[%d-%d-%d %d:%d:%d]:", s.wYear, s.wMonth, s.wDay, s.wHour, s.wMinute, s.wSecond);

			char szInfo[1024];

			try
			{
				va_list header;
				va_start(header, szFormat);
				_vsnprintf(szInfo, 1024, szFormat, header);
				va_end(header);

				szInfo[1023] = 0;

				std::ofstream file(strDbgFileName.c_str(), std::ios_base::app);

				string strOut = string(szTime) + szInfo;

				file << strOut.c_str() << std::endl;
				//file << szTime << szInfo <<std::endl;

				strOut += "\n";
				OutputDebugString(CA2W(strOut.c_str()));

				if (NULL != m_pListBox) {
					m_pListBox->InsertString(0, CA2W(strOut.c_str()));
				}
			}
			catch (...)
			{
			}
			LeaveCriticalSection(&m_crit);
		}

	private:
		CRITICAL_SECTION			m_crit;
	};

};

#define LOGDebug(szFormat, ...)	{ std::string strFormat = std::string("[DEBUG]") + szFormat; \
	Validator::CTrace::Instance().Trace("firewall.log", strFormat.c_str(), __VA_ARGS__); }
#define LOGInfo(szFormat, ...)	{ std::string strFormat = std::string("[INFO]") + szFormat; \
	Validator::CTrace::Instance().Trace("firewall.log", strFormat.c_str(), __VA_ARGS__); }
#define LOGWarn(szFormat, ...)	{ std::string strFormat = std::string("[WARN]") + szFormat; \
	Validator::CTrace::Instance().Trace("firewall.log", strFormat.c_str(), __VA_ARGS__); }
#define LOGError(szFormat, ...)	{ std::string strFormat = std::string("[ERROR]") + szFormat; \
	Validator::CTrace::Instance().Trace("firewall.log", strFormat.c_str(), __VA_ARGS__); }

#endif
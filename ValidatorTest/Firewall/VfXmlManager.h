#ifndef __VFXMLMANAGER__H_
#define __VFXMLMANAGER__H_

#include "tinyxml.h"

class VfTypeLimit;

class VfXmlManager
{
public:
	VfXmlManager() {}
	~VfXmlManager() {}

public:
	static VfXmlManager& Instance()
	{
		static VfXmlManager __Instance;
		return __Instance;
	}

public:
	bool LoadXml(string xmlFileName);

private:
	bool ReloadXml();

	// 载入频率检测
	void LoadFrenquency(TiXmlElement *pElmFrenquency);

	// 载入错误码动作配置
	void LoadCheckErrcode(TiXmlElement *pElmFirewallAction);

	// 载入协议验证列表
	void LoadProtocols(TiXmlElement *pElmProtocols);

	// 载入不同类型的协议验证方法
	//void LoadProtocolSimple(int nProtocolId, TiXmlElement *pElmDATA);
	//void LoadProtocolIgnore(int nProtocolId, TiXmlElement *pElmDATA);
	//void LoadProtocolOnlyLength(int nProtocolId, TiXmlElement *pElmDATA);

	// 解析数值型数据限制
	//void ParseVfTypeLimit(int nProtocol, TiXmlElement *pElmType, VfTypeLimit *pVfTypeValue);

private:
	string m_xmlFileName;

};

#endif
#include "StdAfx.h"
#include "VfXmlManager.h"
#include "Trace.h"
#include "VfProtocol.h"
#include "FirewallManager.h"
#include "StringConvert.h"

bool VfXmlManager::LoadXml( string xmlFileName )
{
	this->m_xmlFileName = xmlFileName;

	return ReloadXml();
}

bool VfXmlManager::ReloadXml()
{
	TiXmlDocument tiXmlDoc;
	if (!tiXmlDoc.LoadFile(m_xmlFileName.c_str()))
	{
		LOGError("载入vf验证文件失败:%s", tiXmlDoc.ErrorDesc());
		return false;
	}

	TiXmlElement *pRoot = tiXmlDoc.FirstChildElement();
	if (NULL == pRoot) 
	{
		LOGError("没有ROOT节点。");
		return false;
	}

	TiXmlElement *pElmFrenquency = pRoot->FirstChildElement("FRENQUENCY");
	if (NULL != pElmFrenquency)
	{
		LoadFrenquency(pElmFrenquency);
	}
	else
	{
		LOGError("没有FRENQUENCY节点。");
	}

	TiXmlElement *pElmCheckErrcode = pRoot->FirstChildElement("CHECKERRCODE");
	if (NULL != pElmCheckErrcode)
	{
		LoadCheckErrcode(pElmCheckErrcode);
	}
	else
	{
		LOGError("没有CHECKERRCODE节点。");
	}

	TiXmlElement *pElmProtocols = pRoot->FirstChildElement("PROTOCOLS");
	if (NULL != pElmProtocols)
	{
		// 载入协议验证列表
		LoadProtocols(pElmProtocols);
	}
	else
	{
		LOGError("没有PROTOCOLS节点。");
	}
	
	return true;
}

void VfXmlManager::LoadCheckErrcode( TiXmlElement *pElmFirewallAction )
{
	map<int, int> mapCheckErrcode;
	map<int, string> mapErrcodeDesc;

	for (TiXmlElement *pElmErrcode = pElmFirewallAction->FirstChildElement(); NULL != pElmErrcode; 
		pElmErrcode = pElmErrcode->NextSiblingElement())
	{
		string strName = pElmErrcode->Value();

		const char *pErrcodeValue = pElmErrcode->Attribute("errcode");
		if (NULL == pErrcodeValue)
		{
			LOGError("CHECKERRCODE中[%s]没有errcode属性。", strName);
			continue;
		}

		const char *pActionValue = pElmErrcode->GetText();
		if (NULL == pActionValue)
		{
			LOGError("CHECKERRCODE中[%s]没有Action内容。", strName);
			continue;
		}

		string strDesc = "";
		const char *pDesc = pElmErrcode->Attribute("desc");
		if (NULL != pDesc)
		{
			strDesc = Utf8ToGbk(pDesc);
		}

		int nErrcode = atoi(pErrcodeValue);
		int nAction = atoi(pActionValue);

		

		if (mapCheckErrcode.find(nErrcode) != mapCheckErrcode.end())
		{
			LOGError("CHECKERRCODE中重复的nErrcode:%d.", nErrcode);
		}
		else
		{
			mapCheckErrcode.insert(make_pair(nErrcode, nAction));
			mapErrcodeDesc.insert(make_pair(nErrcode, strDesc));
		}
	}

	FirewallManager::Instance().SetErrcodeAction(mapCheckErrcode, mapErrcodeDesc);
}

void VfXmlManager::LoadProtocols( TiXmlElement *pElmProtocols )
{
	for (TiXmlElement *pElmProtocol = pElmProtocols->FirstChildElement(); NULL != pElmProtocol; 
		pElmProtocol = pElmProtocol->NextSiblingElement())
	{
		TiXmlElement *pElmID = pElmProtocol->FirstChildElement("ID");
		TiXmlElement *pElmDATA = pElmProtocol->FirstChildElement("DATA");
		if (NULL != pElmID && NULL != pElmDATA)
		{
			// 协议ID
			const char *pProtocolId = pElmID->GetText();
			if (NULL == pProtocolId)
			{
				LOGError("协议ID错误。NULL == pProtocolId");
				continue;
			}

			int nProtocolId = atoi(pProtocolId);

			if (0 == nProtocolId)
			{
				// 0是例子，不加载
				continue;
			}

			if (FirewallManager::Instance().IsExistVfProtocol(nProtocolId))
			{
				LOGError("已经存在协议[%d]。", nProtocolId);
				continue;
			}

			string strDataType = "simple";
			const char *pDataType = pElmDATA->Attribute("type");
			if (NULL != pDataType)
			{
				strDataType = pDataType;
			}

			if ("simple" == strDataType)
			{
				VfProtocolRoot *pVfProtocol = new VfProtocolRoot(nProtocolId);
				pVfProtocol->LoadProtocol(pElmDATA);

				FirewallManager::Instance().AddVfProtocol(nProtocolId, pVfProtocol);
			}
			else if ("onlylength" == strDataType)
			{
				VfProtocolOnlyLength *pVfProtocol = new VfProtocolOnlyLength(nProtocolId);

				pVfProtocol->ParseVfTypeLimit(nProtocolId, pElmDATA);

				FirewallManager::Instance().AddVfProtocol(nProtocolId, pVfProtocol);
			}
			else if ("ignore" == strDataType)
			{
				VfProtocolIgnore *pVfProtocol = new VfProtocolIgnore(nProtocolId);

				FirewallManager::Instance().AddVfProtocol(nProtocolId, pVfProtocol);
			}
			else
			{
				LOGError("协议[%d]未知的Protocol类型。", nProtocolId);
			}

		}	//end if
		
	}	//end for
}

void VfXmlManager::LoadFrenquency( TiXmlElement *pElmFrenquency )
{
	if (NULL == pElmFrenquency)
	{
		LOGError("NULL == pElmFrenquency");
		return;
	}

	bool bOpen = false;

	const char *pOpen = pElmFrenquency->GetText();
	const char *pInterval = pElmFrenquency->Attribute("interval");
	const char *pCount = pElmFrenquency->Attribute("count");
	if (NULL == pOpen)
	{
		LOGError("FRENQUENCY节点没有Value值。");
		return ;
	}

	if (string(pOpen) == "true")
	{
		bOpen = true;
	}

	if (NULL == pInterval || NULL == pCount)
	{
		LOGError("FRENQUENCY节点中interval或者count属性错误。");
		return ;
	}

	int nInterval = atoi(pInterval);
	int nCount = atoi(pCount);

	FirewallManager::Instance().SetFrequency(bOpen, nInterval, nCount);
}

//void VfXmlManager::LoadProtocolSimple( int nProtocolId, TiXmlElement *pElmDATA )
//{
//	if (FirewallManager::Instance().IsExistVfProtocol(nProtocolId))
//	{
//		LOGError("已经存在协议[%d]。", nProtocolId);
//		return ;
//	}
//
//	VfProtocolSimple *pVfProtocol = new VfProtocolSimple(nProtocolId);
//
//	vector<IVfType *> vtVfType(MAX_VT_SIZE);
//
//	// 协议数据
//	for (TiXmlElement *pElmType = pElmDATA->FirstChildElement(); NULL != pElmType;
//		pElmType = pElmType->NextSiblingElement())
//	{
//		string strValue = pElmType->Value();
//
//		int order = 0;
//		const char *pOrder = pElmType->Attribute("order");
//		if (NULL == pOrder)
//		{
//			LOGError("协议[%d]顺序Order错误NULL == pOrder", nProtocolId);
//			continue;
//		}
//
//		order = atoi(pElmType->Attribute("order"));
//		if (order < 0 || order >= MAX_VT_SIZE)
//		{
//			LOGError("协议[%d]顺序Order[%d]错误", nProtocolId, order);
//			continue;
//		}
//
//		if ("INT" == strValue)
//		{
//			VfTypeInt *pVfType = new VfTypeInt();
//			ParseVfTypeLimit(nProtocolId, pElmType, pVfType);
//
//			vtVfType.at(order) = pVfType;
//		}
//		else if ("SHORT" == strValue)
//		{
//			VfTypeShort *pVfType = new VfTypeShort();
//			ParseVfTypeLimit(nProtocolId, pElmType, pVfType);
//
//			vtVfType.at(order) = pVfType;
//		}
//		else if ("BYTE" == strValue)
//		{
//			VfTypeByte *pVfType = new VfTypeByte();
//			ParseVfTypeLimit(nProtocolId, pElmType, pVfType);
//			vtVfType.at(order) = pVfType;
//		}
//		else if ("DWORD" == strValue)
//		{
//			VfTypeDWord *pVfType = new VfTypeDWord();
//			ParseVfTypeLimit(nProtocolId, pElmType, pVfType);
//			vtVfType.at(order) = pVfType;
//		}
//		else if ("STRING" == strValue)
//		{
//			VfTypeString *pVfType = new VfTypeString();
//			ParseVfTypeLimit(nProtocolId, pElmType, pVfType);
//			vtVfType.at(order) = pVfType;
//		}
//		else
//		{
//			LOGError("协议[%d]验证字段类型[%s]错误", nProtocolId, strValue);
//		}
//	}
//
//	// 拷贝到连续的vector中
//	vector<IVfType *> vtFinalVfType;
//	for (int i=0; i<vtVfType.size(); ++i)
//	{
//		if (NULL != vtVfType[i])
//		{
//			vtFinalVfType.push_back(vtVfType[i]);
//		}
//	}
//
//	vtVfType.clear();
//
//	pVfProtocol->SetVfTypes(vtFinalVfType);
//
//	FirewallManager::Instance().AddVfProtocol(nProtocolId, pVfProtocol);
//}

//void VfXmlManager::LoadProtocolIgnore( int nProtocolId, TiXmlElement *pElmDATA )
//{
//	if (FirewallManager::Instance().IsExistVfProtocol(nProtocolId))
//	{
//		LOGError("已经存在协议[%d]。", nProtocolId);
//		return ;
//	}
//
//	VfProtocolIgnore *pVfProtocol = new VfProtocolIgnore(nProtocolId);
//
//	FirewallManager::Instance().AddVfProtocol(nProtocolId, pVfProtocol);
//}

//void VfXmlManager::LoadProtocolOnlyLength( int nProtocolId, TiXmlElement *pElmDATA )
//{
//	if (FirewallManager::Instance().IsExistVfProtocol(nProtocolId))
//	{
//		LOGError("已经存在协议[%d]。", nProtocolId);
//		return ;
//	}
//
//	VfProtocolOnlyLength *pVfProtocol = new VfProtocolOnlyLength(nProtocolId);
//	pVfProtocol->ParseVfTypeLimit(nProtocolId);
//
//	//ParseVfTypeLimit(nProtocolId, pElmDATA, pVfProtocol);
//
//	FirewallManager::Instance().AddVfProtocol(nProtocolId, pVfProtocol);
//}



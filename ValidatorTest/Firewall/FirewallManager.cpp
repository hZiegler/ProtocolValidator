#include "StdAfx.h"
#include "FirewallManager.h"
#include "Trace.h"
#include "VfXmlManager.h"
#include "../USER.h"

volatile bool FirewallManager::g_running;

FirewallManager::FirewallManager(void) 
: m_bOpenFrequency(false), 
m_nInterval(5000),
m_nMaxPackageCount(50)
{
}

FirewallManager::~FirewallManager(void)
{
	ClearAllProtocol();
}

void FirewallManager::Init()
{
	//Register(GAME_START_REQ, &FirewallManager::Check_GAME_START_REQ);

	m_bOpenFrequency = false;
	m_nInterval = 5000;
	m_nMaxPackageCount = 50;

	// 加载前先全部卸载
	m_mapErrcodeAction.clear();

	ClearAllProtocol();

	VfXmlManager::Instance().LoadXml("vf.xml");
}

int FirewallManager::Check_GAME_START_REQ( char *pBuf, int length )
{
	int nStrLen = 0;
	int index = 0;

	// data1：string : szID
	if (length < index + (int)sizeof(BYTE)) 
	{
		return ERRCODE_LENGTH;
	}

	nStrLen = GetByte(pBuf, index);
	if (length < index + nStrLen)
	{
		return ERRCODE_LENGTH;
	}

	index += nStrLen;

	// data2：short : iMyServer
	if (length < index + (int)sizeof(short))
	{
		return ERRCODE_LENGTH;
	}

	short iMyServer = GetShort(pBuf, index);

	// data3：string : szAccountID
	if (length < index + (int)sizeof(BYTE))
	{
		return ERRCODE_LENGTH;
	}

	nStrLen = GetByte(pBuf, index);
	if (length < index + nStrLen)
	{
		return ERRCODE_LENGTH;
	}

	index += nStrLen;

	// 最后多余一个结束字节
	if (length != index + 1)
	{
		return ERRCODE_LENGTH;
	}

	return OK;
}

int FirewallManager::CheckFrenquency( USER *pUser )
{


	return OK;
}

int FirewallManager::Check( BYTE bCommand, char *pBuf, int length, USER *pUser )
{
	int nErrCode = CheckFrenquency(pUser);
	if (OK != nErrCode)
	{
		return nErrCode;
	}

	nErrCode = CheckProtocol(bCommand, pBuf, length, pUser);

	return nErrCode;
}

void FirewallManager::Register( int key, CheckFunc pFunc )
{
	if (m_mapCheckFunc.find(key) == m_mapCheckFunc.end()) 
	{
		m_mapCheckFunc.insert(make_pair(key, pFunc));
	}
	else
	{
		// ERROR重复注册
		LOGError("协议【%d】验证重复注册.", key);
	}
}

bool FirewallManager::IsExistVfProtocol( int nProtocolId )
{
	if (m_mapProtocol.find(nProtocolId) != m_mapProtocol.end())
	{
		return true;
	}
	else 
	{
		return false;
	}
}

bool FirewallManager::AddVfProtocol( int nProtocolId, IVfProtocol *pVfProtocol )
{
	if (m_mapProtocol.find(nProtocolId) == m_mapProtocol.end())
	{
		m_mapProtocol.insert(make_pair(nProtocolId, pVfProtocol));
		return true;
	}
	else 
	{
		LOGError("[!!!]添加重复协议[%d],delete pVfProtocol;pVfProtocol = NULL;释放内存。", nProtocolId);

		delete pVfProtocol;
		pVfProtocol = NULL;
		return false;
	}
}

void FirewallManager::ClearAllProtocol()
{
	map<int, IVfProtocol *>::iterator it = m_mapProtocol.begin();
	for (; it != m_mapProtocol.end(); ++it)
	{
		delete it->second;
	}

	m_mapProtocol.clear();
}

int FirewallManager::CheckProtocol( BYTE bCommand, char *pBuf, int length, USER *pUser )
{
	map<int, IVfProtocol *>::iterator it = m_mapProtocol.find(bCommand);
	if (it == m_mapProtocol.end())
	{
		return ERRCODE_NO_PROTOCOL;
	}

	IVfProtocol *pVfProtocol = it->second;

	int retVal = pVfProtocol->Check(pBuf, length);

	return retVal;
}

void FirewallManager::SetErrcodeAction( map<int, int> mapErrcodeAction, map<int, string> mapErrcodeDesc )
{
	m_mapErrcodeAction = mapErrcodeAction;
	m_mapErrcodeDesc = mapErrcodeDesc;
}

int FirewallManager::GetActionByErrcode( int nErrcode )
{
	map<int, int>::iterator it = m_mapErrcodeAction.find(nErrcode);
	if (it != m_mapErrcodeAction.end())
	{
		return it->second;
	}

	// 没有找到范围Empty
	return EMPTY;
}

std::string FirewallManager::GetDescByErrcode( int nErrcode )
{
	map<int, std::string>::iterator it = m_mapErrcodeDesc.find(nErrcode);
	if (it != m_mapErrcodeDesc.end())
	{
		return it->second;
	}

	// 没有找到范围Empty
	return "";
}

void FirewallManager::SetFrequency( bool bOpen, int nInterval, int nPackageCount )
{
	this->m_bOpenFrequency = bOpen;
	this->m_nInterval = nInterval;
	this->m_nMaxPackageCount = nPackageCount;
}



BOOL WriteArrayToLog(TCHAR *pData,int iDataLen)
{
	TCHAR szMsg[8192] = {0};
	strcat_s(szMsg, "HEX data[%d]:");
	for(int i = 0; i < (int)iDataLen;i++)
	{
		TCHAR szTemp[32] = {0};
		sprintf_s(szTemp,("%02x,"),(unsigned char)pData[i]);
		strcat_s(szMsg,szTemp);
		//if((i+1)%20==0)
		//	strcat(szMsg,"\r\n");
	}
	LOGDebug(szMsg, iDataLen);
	return TRUE;
}


#include "StdAfx.h"
#include "VfTypes.h"

#include "FirewallManager.h"
#include "Trace.h"

VfTypeLimit::VfTypeLimit() 
	: m_pChildVfProtocolFor(NULL),
	m_pChildVfProtocolElseIf(NULL)
{
	
}

VfTypeLimit::~VfTypeLimit()
{
	if (NULL != m_pChildVfProtocolFor)
	{
		delete m_pChildVfProtocolFor;
		m_pChildVfProtocolFor = NULL;
	}

	if (NULL != m_pChildVfProtocolElseIf)
	{
		delete m_pChildVfProtocolElseIf;
		m_pChildVfProtocolElseIf = NULL;
	}

	for (map<long, VfProtocolIf *>::iterator it=m_mapChildrenVfProtocolIf.begin(); 
		it != m_mapChildrenVfProtocolIf.end(); ++it)
	{
		delete it->second;
	}
	m_mapChildrenVfProtocolIf.clear();
}

long VfTypeLimit::GetMin()
{
	if (HasMin())
	{
		return m_mapLimit.find(KEY_MIN)->second;
	}

	return 0L;
}

long VfTypeLimit::GetMax()
{
	if (HasMin())
	{
		return m_mapLimit.find(KEY_MAX)->second;
	}

	return 0L;
}

bool VfTypeLimit::HasMin()
{
	return (m_mapLimit.find(KEY_MIN) != m_mapLimit.end());
}

bool VfTypeLimit::HasMax()
{ 
	return (m_mapLimit.find(KEY_MAX) != m_mapLimit.end()); 
}

bool VfTypeLimit::HasIf()
{ 
	return (m_mapLimit.find(KEY_IF) != m_mapLimit.end()); 
}

bool VfTypeLimit::HasElseIf()
{
	return (m_mapLimit.find(KEY_ELSE_IF) != m_mapLimit.end()); 
}

bool VfTypeLimit::HasFor()
{
	return (m_mapLimit.find(KEY_FOR) != m_mapLimit.end()); 
}

void VfTypeLimit::ParseVfTypeLimit( int nProtocol, TiXmlElement *pElmType )
{
	// 限制条件
	for (TiXmlElement *pElmLimit = pElmType->FirstChildElement(); NULL != pElmLimit;
		pElmLimit = pElmLimit->NextSiblingElement())
	{
		string strValue = pElmLimit->Value();

		if ("IF" == strValue)
		{
			const char *pEqualsValue = pElmLimit->Attribute("equals");
			if (NULL == pEqualsValue)
			{
				LOGError("协议[%d]IF标签Equals异常。", nProtocol);
				continue;
			}
			long n = atol(pEqualsValue);

			if (m_mapChildrenVfProtocolIf.find(n) != m_mapChildrenVfProtocolIf.end())
			{
				LOGError("协议[%d]IF标签equal[%d]值已经存在。", nProtocol, n);
				continue;
			}

			// 设置标志
			SetIf();
			
			VfProtocolIf *pVfProtocolSub = new VfProtocolIf();
			pVfProtocolSub->LoadProtocol(pElmLimit);

			m_mapChildrenVfProtocolIf.insert(make_pair(n, pVfProtocolSub));
		}
		else if ("ELSEIF" == strValue) 
		{
			if (NULL != m_pChildVfProtocolElseIf)
			{
				LOGError("协议[%d]ELSEIF标签已经存在。", nProtocol);
				continue;
			}

			// 设置标志
			SetElseIf();

			VfProtocolElseIf *pVfProtocolSub = new VfProtocolElseIf();
			pVfProtocolSub->LoadProtocol(pElmLimit);

			m_pChildVfProtocolElseIf = pVfProtocolSub;
		}
		else if ("FOR" == strValue)
		{
			if (NULL != m_pChildVfProtocolFor)
			{
				LOGError("协议[%d]FOR标签已经存在。", nProtocol);
				continue;
			}

			// 设置标志
			SetFor();

			VfProtocolFor *pVfProtocolFor = new VfProtocolFor();
			pVfProtocolFor->LoadProtocol(pElmLimit);

			m_pChildVfProtocolFor = pVfProtocolFor;
		}
		else
		{
			long n = 0L;
			const char *pValue = pElmLimit->GetText();
			if (NULL == pValue)
			{
				LOGError("协议[%d]限制条件数值为空");
				continue;
			}

			n = atol(pValue);

			if ("MIN" == strValue)
			{
				SetMinLimit(n);
			}
			else if ("MAX" == strValue)
			{
				SetMaxLimit(n);
			}
		}	//end if else
	}	//end for
}

int VfTypeLimit::CheckLimit( long nVal )
{
	if (HasMin())
	{
		long nMin = GetMin();
		if (nVal < nMin)
		{
			return ERRCODE_OUT_OF_RANGE;
		}
	}

	if (HasMax())
	{
		long nMax = GetMax();
		if (nVal > nMax)
		{
			return ERRCODE_OUT_OF_RANGE;
		}
	}

	return OK;
}

int VfTypeLimit::Check( long nCheckValue, int &index, TCHAR *pBuf, int length )
{
	int nErrCode = CheckLimit(nCheckValue);
	if (OK != nErrCode)
	{
		return nErrCode;
	}

	if (HasIf())
	{
		map<long, VfProtocolIf *>::iterator it = m_mapChildrenVfProtocolIf.find(nCheckValue);
		if (it != m_mapChildrenVfProtocolIf.end())
		{
			VfProtocolIf *pVfProtocolSub = it->second;
			if (NULL != pVfProtocolSub)
			{
				nErrCode = pVfProtocolSub->Check(index, pBuf, length);
				if (OK != nErrCode)
				{
					return nErrCode;
				}
			}
		}
		else 
		{
			if (HasElseIf())
			{
				if (NULL == m_pChildVfProtocolElseIf)
				{
					LOGError("NULL == m_pChildVfProtocolElseIf");
				}
				else
				{
					nErrCode = m_pChildVfProtocolElseIf->Check(index, pBuf, length);
					if (OK != nErrCode)
					{
						return nErrCode;
					}
				}
			}
		}
	}
	
	if (HasFor())
	{
		if (NULL == m_pChildVfProtocolFor)
		{
			LOGError("NULL == m_pChildrenVfProtocolFor");
		}
		else 
		{
			for (int i=0; i<nCheckValue; ++i)
			{
				nErrCode = m_pChildVfProtocolFor->Check(index, pBuf, length);
				if (OK != nErrCode)
				{
					return nErrCode;
				}
			}
		}
	}

	return OK;
}





int VfTypeInt::Check( int &index, TCHAR *pBuf, int length )
{
	if (length < index + (int)sizeof(int))
	{
		return ERRCODE_LENGTH;
	}

	int n = GetInt(pBuf, index);

	return VfTypeLimit::Check(n, index, pBuf, length);
}

int VfTypeShort::Check( int &index, TCHAR *pBuf, int length )
{
	// data2：short : iMyServer
	if (length < index + (int)sizeof(short))
	{
		return ERRCODE_LENGTH;
	}

	short n = GetShort(pBuf, index);

	return VfTypeLimit::Check(n, index, pBuf, length);
}

int VfTypeByte::Check( int &index, TCHAR *pBuf, int length )
{
	if (length < index + (int)sizeof(BYTE))
	{
		return ERRCODE_LENGTH;
	}

	int n = GetByte(pBuf, index);

	return VfTypeLimit::Check(n, index, pBuf, length);
}

int VfTypeDWord::Check( int &index, TCHAR *pBuf, int length )
{
	if (length < index + (int)sizeof(DWORD))
	{
		return ERRCODE_LENGTH;
	}

	int n = GetDWORD(pBuf, index);

	return VfTypeLimit::Check(n, index, pBuf, length);
}

int VfTypeString::Check( int &index, TCHAR *pBuf, int length )
{
	// data1：string : szID
	if (length < index + (int)sizeof(BYTE))
	{
		return ERRCODE_LENGTH;
	}

	int nStrLen = GetByte(pBuf, index);
	if (length < index + nStrLen)
	{
		return ERRCODE_LENGTH;
	}

	index += nStrLen;

	return VfTypeLimit::Check(nStrLen, index, pBuf, length);
}
#ifndef __IVFTYPE__
#define __IVFTYPE__

#include "tinyxml.h"

class VfProtocolRoot;
class VfProtocolIf;
class VfProtocolElseIf;
class VfProtocolFor;


enum EnumVfType
{
	VfEmpty = 0,
	VfInt = 1,
	VfShort = 2,
	VfString = 3,
	VfByte = 4,
	VfDWord = 5,
};

class IVfType
{
public:
	virtual ~IVfType() {}

public:
	virtual EnumVfType GetType() = 0;

	virtual int Check(int &index, TCHAR *pBuf, int length) = 0;


};

// 数值类型
class VfTypeLimit : public IVfType
{
#define KEY_MIN	0
#define KEY_MAX	1
#define KEY_IF 2
#define KEY_ELSE_IF 3
#define KEY_FOR	4

public:
	VfTypeLimit();
	~VfTypeLimit();
	void ParseVfTypeLimit(int nProtocol, TiXmlElement *pElmType);

protected:
	bool HasMin();
	bool HasMax();
	bool HasIf();
	bool HasElseIf();
	bool HasFor();

	long GetMin();
	long GetMax();

	void SetMinLimit(long n) { m_mapLimit[KEY_MIN] = n; }
	void SetMaxLimit(long n) { m_mapLimit[KEY_MAX] = n; }
	void SetIf() { m_mapLimit[KEY_IF] = 1; }
	void SetElseIf() { m_mapLimit[KEY_ELSE_IF] = 1; }
	void SetFor() { m_mapLimit[KEY_FOR] = 1; }
	
	int Check(long nCheckValue, int &index, TCHAR *pBuf, int length);

private:
	virtual int CheckLimit(long nVal);


protected:
	// 限制key=0代表最小值，key=1代表最大值
	map<int, long> m_mapLimit;
	// 嵌套协议IF
	map<long, VfProtocolIf *> m_mapChildrenVfProtocolIf;
	// ElseIf
	VfProtocolElseIf *m_pChildVfProtocolElseIf;
	// 嵌套协议FOR
	VfProtocolFor *m_pChildVfProtocolFor;
};

class VfTypeInt : public VfTypeLimit
{
public:
	virtual EnumVfType GetType() { return VfInt; }
	virtual int Check(int &index, TCHAR *pBuf, int length);
};

class VfTypeShort : public VfTypeLimit
{
public:
	virtual EnumVfType GetType() { return VfShort; }
	virtual int Check(int &index, TCHAR *pBuf, int length);
};

class VfTypeByte : public VfTypeLimit
{
public:
	virtual EnumVfType GetType() { return VfInt; }
	virtual int Check(int &index, TCHAR *pBuf, int length);
};

class VfTypeDWord : public VfTypeLimit
{
public:
	virtual EnumVfType GetType() { return VfDWord; }
	virtual int Check(int &index, TCHAR *pBuf, int length);
};

class VfTypeString : public VfTypeLimit
{
public:
	virtual EnumVfType GetType() { return VfString; }
	virtual int Check(int &index, TCHAR *pBuf, int length);
};



#endif

// ValidatorTestDlg.cpp: 实现文件
//

#include "stdafx.h"
#include "ValidatorTest.h"
#include "ValidatorTestDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CValidatorTestDlg 对话框



CValidatorTestDlg::CValidatorTestDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_VALIDATORTEST_DIALOG, pParent)
	, m_strValue(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CValidatorTestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_strValue);
	DDX_Control(pDX, IDC_LIST1, m_listBox);
}

BEGIN_MESSAGE_MAP(CValidatorTestDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDOK, &CValidatorTestDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_CLEAR_LOG, &CValidatorTestDlg::OnBnClickedClearLog)
END_MESSAGE_MAP()


// CValidatorTestDlg 消息处理程序

BOOL CValidatorTestDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
	Validator::CTrace::Instance().m_pListBox = &m_listBox;

	LOGInfo("协议验证器启动-%s-%s", __DATE__, __TIME__);
	FirewallManager::Instance().Init();
	FirewallManager::g_running = true;

	//int nErrcode = FirewallManager::Instance().Check(command, pBuf + index, len - 5, this);
	//if (OK != nErrcode)
	//{
	//	// 这里根据各种验证情况进行相应处理
	//	int nAction = FirewallManager::Instance().GetActionByErrcode(nErrcode);
	//	std::string strDesc = FirewallManager::Instance().GetDescByErrcode(nErrcode);

	//	// 打印协议数据
	//	bool bLogHex = false;

	//	switch (nAction)
	//	{
	//	case EMPTY:
	//		break;
	//	case ACTION_LOG:
	//		bLogHex = true;
	//		LOGError("协议[%d]出现错误:%d-%s。[%d][%s-%s]", command, nErrcode, strDesc.c_str(),
	//			m_Socket, m_strAccount, m_strUserID);
	//		break;
	//	case ACTION_KICK:
	//		bLogHex = true;
	//		LOGError("协议[%d]出现错误:%d-%s，踢掉客户端。[%d][%s-%s]", command, nErrcode, strDesc.c_str(),
	//			m_Socket, m_strAccount, m_strUserID);
	//		SendSystemMsg("网络断开！\n怀疑你执行了非法操作！", SYSTEM_SPECIAL, TO_ME);
	//		SoftClose();
	//		break;
	//	case ACTION_FILTER_LOG:
	//		bLogHex = true;
	//		LOGError("协议[%d]出现错误:%d-%s，返回不执行协议。[%d][%s-%s]", command, nErrcode, strDesc.c_str(),
	//			m_Socket, m_strAccount, m_strUserID);
	//		return;
	//	case ACTION_FILTER_NO_LOG:
	//		return;
	//	default:
	//	{
	//		bLogHex = true;
	//		LOGError("协议[%d]出现错误:%d-%s,未定义的动作[%d]类型.", command, nErrcode, strDesc.c_str(), nAction);
	//	}
	//	}

	//	if (bLogHex)
	//	{
	//		WriteArrayToLog(pBuf, len);
	//	}
	//}


	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CValidatorTestDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CValidatorTestDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CValidatorTestDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CValidatorTestDlg::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码

	UpdateData(TRUE);
	CString strContext;
	GetDlgItemText(IDC_EDIT1, strContext);

	string strContent = CW2A(strContext.GetString());

	//strContent = "3c,01,00,00,37,01,0c,00";

	//LOGInfo("m_strValue:%s", strContent.c_str());

	// 转化字符串数据为二进制数据
	vector<string> vtString;
	SplitString(strContent, vtString, string(","));

	//3c,01,00,00,37,01,0c,00

	

	int i = 0;
	for (string s : vtString) {
		//int n = HextoDec(s.c_str(), s.length());
		char *pEnd;
		long int n = strtol(s.c_str(), &pEnd, 16);
		char c = (char)n;
		m_szBuf[i++] = c;
	}

	int len = i;

	if (len <= 5) {
		LOGError("请输入足够的验证数据。当前数据长度：%d。", len);
		return;
	}

	int command = (unsigned char)m_szBuf[4];
	char *pBuf = m_szBuf;

	LOGInfo("协议总长度[%d]包头ID[%d]内容长度[%d].", len, command, len - 5);

	WriteArrayToLog(pBuf, len);

	int nErrcode = FirewallManager::Instance().Check(command, pBuf + 5, len - 5, nullptr);
	if (OK != nErrcode)
	{
		// 这里根据各种验证情况进行相应处理
		int nAction = FirewallManager::Instance().GetActionByErrcode(nErrcode);
		std::string strDesc = FirewallManager::Instance().GetDescByErrcode(nErrcode);

		LOGError("验证失败：ACTION[%d]协议[%d]出现错误:%d-%s。", nAction, command, nErrcode, strDesc.c_str());
	}
	else
	{
		LOGInfo("验证通过！");
	}
}


void CValidatorTestDlg::OnBnClickedClearLog()
{
	// TODO: 在此添加控件通知处理程序代码
	m_listBox.ResetContent();
}


BOOL CValidatorTestDlg::DestroyWindow()
{
	// TODO: 在此添加专用代码和/或调用基类

	return CDialogEx::DestroyWindow();
}
